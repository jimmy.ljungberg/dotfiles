# Dotfiles

Jag använder mig av [chezmoi](https://www.chezmoi.io/) för att hantera mina konfigurationsfiler.

## Installation

```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/kilathaar/dotfiles.git
```

Om du är jag, så vill du nog även justera git-konfigurationen och byta till SSH för git

```bash
chezmoi cd && git remote set-url origin git@gitlab.com:kilathaar/dotfiles.git
```

## Användning

chezmoi fungerar mycket som en wrapper för git.

### Byt till git-katalog

```bash
chezmoi cd
```

Nu står du i den katalog som hanteras av git och vanliga git-kommandon gäller.

### Uppdatera lokala dot-filer

```bash
chezmoi update
```

### Lägg till ny fil

```bash
chezmoi add <sökväg till fil>
```

