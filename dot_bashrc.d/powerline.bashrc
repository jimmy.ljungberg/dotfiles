if ! command -v powerline-daemon &> /dev/null; then
	return
fi

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1

if [ -f /usr/share/powerline/bash/powerline.sh ]; then
	. /usr/share/powerline/bash/powerline.sh
elif [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
	. /usr/share/powerline/bindings/bash/powerline.sh
fi
