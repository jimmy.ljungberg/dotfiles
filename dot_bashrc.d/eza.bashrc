if ! command -v eza &> /dev/null; then
    return
fi

alias eza="eza --icons=auto"
alias ls="eza"
