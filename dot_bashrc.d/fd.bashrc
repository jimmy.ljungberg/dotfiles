if ! command -v fdfind &> /dev/null; then
    return
fi

alias fd="fdfind"
