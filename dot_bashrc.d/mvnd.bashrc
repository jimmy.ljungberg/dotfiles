if ! command -v mvnd &> /dev/null; then
    return
fi

source <(mvnd --completion bash)
