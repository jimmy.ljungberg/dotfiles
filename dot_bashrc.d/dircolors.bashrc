if ! command -v dircolors &> /dev/null; then
    return
fi

test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
