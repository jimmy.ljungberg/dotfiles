if ! command -v docker &> /dev/null; then
    return
fi

alias dcd="docker compose down"
alias dcu="docker compose up"
