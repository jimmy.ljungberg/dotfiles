if ! command -v tree &> /dev/null; then
    return
fi

complete -d tree

alias tree="tree --gitignore"
