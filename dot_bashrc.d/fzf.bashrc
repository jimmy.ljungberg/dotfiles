if ! test -d ${HOME}/.local/fzf; then
    return
fi

if [[ ! "$PATH" == *${HOME}/.local/fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}${HOME}/.local/fzf/bin"
fi

eval "$(fzf --bash)"

source "${HOME}/.local/fzf/shell/completion.bash"
source "${HOME}/.local/fzf/shell/key-bindings.bash"

if command -v batcat &> /dev/null; then
  export FZF_DEFAULT_OPTS="--preview 'bat --color=always {}'"    
fi

if command -v fdfind &> /dev/null; then
  export FZF_DEFAULT_COMMAND="fdfind --type f"
fi
